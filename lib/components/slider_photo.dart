import 'package:flutter/material.dart';

class SliderPhoto extends StatelessWidget {
  final String photoUrl;
  double photoWidth;
  double photoHeight;

  SliderPhoto({super.key,
    required this.photoUrl,
    this.photoWidth = 200,
    this.photoHeight = 200
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset(photoUrl, width: photoWidth, height: photoHeight),
    );
  }
}
