# [연습용] 패션쇼핑몰 APP

***
### LANGUAGE
```
* Dart
* Flutter
```
***
### 기능
```
    - carousel_slider 패키지 활용
    - 광고용 배너 보여주기
    - MockUp 데이터 활용하여 리스트 보여주기
```
![covid_clinic](./assets/musinsa2.png)