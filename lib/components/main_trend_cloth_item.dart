import 'package:flutter/material.dart';

class MainTrendClothItem extends StatelessWidget {
  final String photoUrl;
  final String goodsName;
  final int goodsPrice;


  const MainTrendClothItem({super.key, required this.photoUrl, required this.goodsName, required this.goodsPrice});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Image.asset(photoUrl, width: 200, height: 200),
          Text(goodsName, style: TextStyle(letterSpacing: 1.0),),
          Text(goodsPrice.toString() + '원')
        ],
      ),
    );
  }
}
