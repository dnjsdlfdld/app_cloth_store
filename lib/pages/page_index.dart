import 'package:app_cloth_store/pages/page_man_cloth.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex>
    with SingleTickerProviderStateMixin {

  late TabController _tabController;

  static const _kTabPages = <Widget>[
    Center(child: Icon(Icons.forum, size: 64.0, color: Colors.blue)),
    Center(child: Icon(Icons.alarm, size: 64.0, color: Colors.cyan)),
    PageManCloth(),
    Center(child: Icon(Icons.forum, size: 64.0, color: Colors.blue)),
    Center(child: Icon(Icons.forum, size: 64.0, color: Colors.blue)),
  ];
  static const _kTabs = <Tab>[
    Tab(icon: Icon(Icons.adb_rounded), text: '커뮤니티'),
    Tab(icon: Icon(Icons.access_alarms_sharp), text: '타임세일'),
    Tab(icon: Icon(Icons.home), text: '홈'),
    Tab(icon: Icon(Icons.forum), text: '좋아요'),
    Tab(icon: Icon(Icons.forum), text: '내 정보'),
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: _kTabPages.length,
      vsync: this,
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('MUSINSA', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700)),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Container(
        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
        child: TabBarView(
          controller: _tabController,
          children: _kTabPages,
        ),
      ),
      bottomNavigationBar: Material(
        color: Colors.black,
        child: TabBar(
          tabs: _kTabs,
          controller: _tabController,
        ),
      ),
    );
  }
}
