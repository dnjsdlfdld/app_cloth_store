import 'package:app_cloth_store/components/main_trend_cloth_item.dart';
import 'package:app_cloth_store/config/config_trand_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class PageManCloth extends StatefulWidget {
  const PageManCloth({Key? key}) : super(key: key);



  @override
  State<PageManCloth> createState() => _PageManClothState();
}

final List<String> imgList = [
  'assets/musinsa.png',
  'assets/sale~a.jpg',
];

final List<Widget> imageSliders = imgList
    .map((item) => Container(
          child: Container(
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: Stack(
                  children: <Widget>[
                    Image.asset(item, fit: BoxFit.cover, width: 1000.0),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(200, 0, 0, 0),
                              Color.fromARGB(0, 0, 0, 0)
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20.0),
                        child: Text(
                          '이미지 ${imgList.indexOf(item)}',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ))
    .toList();

class _PageManClothState extends State<PageManCloth> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(20, 0, 0, 10),
              child: Row(
                children: [
                  Text('이달의 HOT',
                      style: TextStyle(
                        fontFamily: 'Changa-Regular',
                        fontSize: 20,
                        letterSpacing: 1.5,
                        fontWeight: FontWeight.w600,
                      ))
                ],
              ),
            ),
            CarouselSlider(
              items: imageSliders,
              options: CarouselOptions(
                autoPlay: true,
                aspectRatio: 2.0,
                enlargeCenterPage: true,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
              child: Row(
                children: [
                  Text('패션 트랜드',
                    style: TextStyle(
                      fontFamily: 'Changa-Regular',
                      fontSize: 20,
                      letterSpacing: 1.5,
                      fontWeight: FontWeight.w600,
                    ))
              ],
            ),
            ),
            SizedBox(height: 20),
            Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 0, 30, 0),
                        child: Row(
                          children: [
                            for (int i = 0; i < trendClothItems.length; i++)
                              MainTrendClothItem(
                                  photoUrl: trendClothItems[i].photoUrl,
                                  goodsName: trendClothItems[i].goodsName,
                                  goodsPrice: trendClothItems[i].goodsPrice)
                          ],
                        ),
                      ),
                    ],

                  )
                )
              ),
            SizedBox(height: 50),
            Container(
              margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
              child: Row(
                children: [
                  Text('잠깐 ! 무신사 소식',
                      style: TextStyle(
                        fontFamily: 'Changa-Regular',
                        fontSize: 20,
                        letterSpacing: 1.2,
                        fontWeight: FontWeight.w600,
                      ))
                ],
              ),
            ),
          ],
        )),
    );
  }
}
