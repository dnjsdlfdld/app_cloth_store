
import 'package:app_cloth_store/model/trend_cloth_item.dart';

final List<TrendClothItem> trendClothItems = [
  TrendClothItem('assets/cloth1.jpg', '타미힐피거 맨투맨', 34000),
  TrendClothItem('assets/cloth2.png', '나이키 스우시 맨투맨', 40000),
  TrendClothItem('assets/cloth5.jpg', '톰브라운 Line4 맨투맨', 680000),
  TrendClothItem('assets/cloth4.jpg', '루이비통 PXG에디션 맨투맨', 1700000),
  TrendClothItem('assets/cloth3.jpg', '메종키츠네 맨투맨', 360000)
];